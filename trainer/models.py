from tensorflow.keras.applications.vgg16 import VGG16
from tensorflow.keras.applications.vgg19 import VGG19
from tensorflow.keras.layers import Dropout, Flatten, Dense, Input, MaxPool2D, GlobalAveragePooling2D, LeakyReLU, BatchNormalization
from tensorflow.keras.layers import Lambda, Conv2D, concatenate, ZeroPadding2D, Layer, MaxPooling2D, GaussianNoise
from tensorflow.keras.models import Model
from tensorflow.image import resize_images
import tensorflow.keras.backend as K
from .utils import get_layers_output_by_name


def mildnet_vgg16(embedding_size, input_shape):
    
    vgg_model = VGG16(weights="imagenet", include_top=False, input_shape=input_shape)

    for layer in vgg_model.layers[:10]:
        layer.trainable = False

    mid_layer_names = ["block1_pool", "block2_pool", "block3_pool", "block4_pool"]
    mid_layer_outputs = get_layers_output_by_name(vgg_model, mid_layer_names)

    convnet_output = GlobalAveragePooling2D(name='ap_1')(vgg_model.output)
    
    for ldx, (layer_name, output) in enumerate(mid_layer_outputs.items()):
        output = GlobalAveragePooling2D(name='ap_'+str(ldx+2))(output)
        convnet_output = concatenate([convnet_output, output], name='concat_'+str(ldx+1))

    convnet_output = Dense(2048, activation='relu', name='dense_1', kernel_initializer='he_uniform')(convnet_output)
    convnet_output = Dropout(0.6)(convnet_output)
    convnet_output = Dense(embedding_size, activation='relu', name='dense_2', kernel_initializer='he_uniform')(convnet_output)
    convnet_output = Lambda(lambda x: K.l2_normalize(x, axis=1), name='final_emb')(convnet_output)

    return Model(inputs=vgg_model.input, outputs=convnet_output)


def ranknet(embedding_size, input_shape):
    
    vgg_model = VGG19(weights="imagenet", include_top=False, input_shape=input_shape)
    convnet_output = GlobalAveragePooling2D()(vgg_model.output)
    convnet_output = Dense(4096, activation='relu', kernel_initializer='he_uniform')(convnet_output)
    convnet_output = Dropout(0.5)(convnet_output)
    convnet_output = Dense(4096, activation='relu', kernel_initializer='he_uniform')(convnet_output)
    convnet_output = Dropout(0.5)(convnet_output)
    convnet_output = Lambda(lambda x: K.l2_normalize(x, axis=1))(convnet_output)

    s1 = MaxPool2D(pool_size=(4, 4), strides=(4, 4), padding='valid')(vgg_model.input)
    s1 = ZeroPadding2D(padding=(4, 4), data_format=None)(s1)
    s1 = Conv2D(96, kernel_size=(8, 8), strides=(4, 4), padding='valid', kernel_initializer='he_uniform')(s1)
    s1 = ZeroPadding2D(padding=(2, 2), data_format=None)(s1)
    s1 = MaxPool2D(pool_size=(7, 7), strides=(4, 4), padding='valid')(s1)
    s1 = Flatten()(s1)

    s2 = MaxPool2D(pool_size=(8, 8), strides=(8, 8), padding='valid')(vgg_model.input)
    s2 = ZeroPadding2D(padding=(4, 4), data_format=None)(s2)
    s2 = Conv2D(96, kernel_size=(8, 8), strides=(4, 4), padding='valid', kernel_initializer='he_uniform')(s2)
    s2 = ZeroPadding2D(padding=(1, 1), data_format=None)(s2)
    s2 = MaxPool2D(pool_size=(3, 3), strides=(2, 2), padding='valid')(s2)
    s2 = Flatten()(s2)

    merge_one = concatenate([s1, s2])
    merge_one_norm = Lambda(lambda x: K.l2_normalize(x, axis=1))(merge_one)
    merge_two = concatenate([merge_one_norm, convnet_output], axis=1)
    emb = Dense(embedding_size, kernel_initializer='he_uniform')(merge_two)
    l2_norm_final = Lambda(lambda x: K.l2_normalize(x, axis=1))(emb)

    return Model(inputs=vgg_model.input, outputs=l2_norm_final)

def mildnet_color_vgg16(embedding_size, input_shape):
    
    vgg_model = VGG16(weights="imagenet", include_top=False, input_shape=input_shape)
    
    X_color_input = Lambda(lambda images : resize_images(images, (64, 64)))(vgg_model.input)
    X = Conv2D(32, kernel_size=(3, 3), strides=(2, 2), kernel_initializer='he_uniform')(X_color_input)
    X = GaussianNoise(0.01)(X)
    X = BatchNormalization()(X)
    X = LeakyReLU(alpha=0.1)(X)
    
    X = Conv2D(64, kernel_size=(3, 3), strides=(2, 2), kernel_initializer='he_uniform')(X)
    #X = GaussianNoise(0.1)(X)
    X = BatchNormalization()(X)
    X = LeakyReLU(alpha=0.1)(X)
    
    X = Conv2D(96, kernel_size=(3, 3), strides=(2, 2), kernel_initializer='he_uniform')(X)
    #X = GaussianNoise(0.1)(X)
    X = BatchNormalization()(X)
    X = LeakyReLU(alpha=0.1)(X)
    
    X = Flatten()(X)
    X = Dense(128, kernel_initializer='he_uniform')(X)
    X = LeakyReLU(alpha=0.1)(X)
    
    for layer in vgg_model.layers[:10]:
        layer.trainable = False

    mid_layer_names = ["block1_pool", "block2_pool", "block3_pool", "block4_pool"]
    mid_layer_outputs = get_layers_output_by_name(vgg_model, mid_layer_names)

    convnet_output = GlobalAveragePooling2D(name='ap_1')(vgg_model.output)
    
    for ldx, (layer_name, output) in enumerate(mid_layer_outputs.items()):
        output = GlobalAveragePooling2D(name='ap_'+str(ldx+2))(output)
        convnet_output = concatenate([convnet_output, output], name='concat_'+str(ldx+1))

    convnet_output = Dense(2048, activation='linear', kernel_initializer='he_uniform')(convnet_output)
    convnet_output = BatchNormalization()(convnet_output)
    convnet_output = LeakyReLU(alpha=0.1)(convnet_output)
    convnet_output = Dropout(0.25)(convnet_output)
    
    convnet_output = concatenate([convnet_output, X], name='concat_last')
    
    convnet_output = Dense(embedding_size, activation='linear', kernel_initializer='he_uniform')(convnet_output)
    convnet_output = BatchNormalization()(convnet_output)
    convnet_output = LeakyReLU(alpha=0.1)(convnet_output)
    convnet_output = Lambda(lambda out: K.l2_normalize(out, axis=1), name='final_emb')(convnet_output)

    return Model(inputs=vgg_model.input, outputs=convnet_output)


models = {'mildnet': mildnet_vgg16, 'ranknet': ranknet, 'mildnet_color': mildnet_color_vgg16}
