import tensorflow.keras.backend as K
import logging


def get_layers_output_by_name(model, layer_names):
    return {v: model.get_layer(v).output for v in layer_names}


def print_trainable_counts(model, mode='log'):
    
    trainable_count = int(sum([K.count_params(p) for p in set(model.trainable_weights)]))
    non_trainable_count = int(sum([K.count_params(p) for p in set(model.non_trainable_weights)]))
    return trainable_count, non_trainable_count
